/** main.js **/
/*!
* screenfull
* v5.1.0 - 2020-12-24
* (c) Sindre Sorhus; MIT License
*/

!function(){"use strict";var c="undefined"!=typeof window&&void 0!==window.document?window.document:{},e="undefined"!=typeof module&&module.exports,s=function(){for(var e,n=[["requestFullscreen","exitFullscreen","fullscreenElement","fullscreenEnabled","fullscreenchange","fullscreenerror"],["webkitRequestFullscreen","webkitExitFullscreen","webkitFullscreenElement","webkitFullscreenEnabled","webkitfullscreenchange","webkitfullscreenerror"],["webkitRequestFullScreen","webkitCancelFullScreen","webkitCurrentFullScreenElement","webkitCancelFullScreen","webkitfullscreenchange","webkitfullscreenerror"],["mozRequestFullScreen","mozCancelFullScreen","mozFullScreenElement","mozFullScreenEnabled","mozfullscreenchange","mozfullscreenerror"],["msRequestFullscreen","msExitFullscreen","msFullscreenElement","msFullscreenEnabled","MSFullscreenChange","MSFullscreenError"]],l=0,r=n.length,t={};l<r;l++)if((e=n[l])&&e[1]in c){for(l=0;l<e.length;l++)t[n[0][l]]=e[l];return t}return!1}(),l={change:s.fullscreenchange,error:s.fullscreenerror},n={request:function(t,u){return new Promise(function(e,n){var l=function(){this.off("change",l),e()}.bind(this);this.on("change",l);var r=(t=t||c.documentElement)[s.requestFullscreen](u);r instanceof Promise&&r.then(l).catch(n)}.bind(this))},exit:function(){return new Promise(function(e,n){var l,r;this.isFullscreen?(l=function(){this.off("change",l),e()}.bind(this),this.on("change",l),(r=c[s.exitFullscreen]())instanceof Promise&&r.then(l).catch(n)):e()}.bind(this))},toggle:function(e,n){return this.isFullscreen?this.exit():this.request(e,n)},onchange:function(e){this.on("change",e)},onerror:function(e){this.on("error",e)},on:function(e,n){e=l[e];e&&c.addEventListener(e,n,!1)},off:function(e,n){e=l[e];e&&c.removeEventListener(e,n,!1)},raw:s};s?(Object.defineProperties(n,{isFullscreen:{get:function(){return Boolean(c[s.fullscreenElement])}},element:{enumerable:!0,get:function(){return c[s.fullscreenElement]}},isEnabled:{enumerable:!0,get:function(){return Boolean(c[s.fullscreenEnabled])}}}),e?module.exports=n:window.screenfull=n):e?module.exports={isEnabled:!1}:window.screenfull={isEnabled:!1}}();

/* libs End */

/* site code*/
window.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
});
// pass audio element after domload only
async function setupAudioOutput(audio) {
  const devices = await navigator.mediaDevices.enumerateDevices();
  const audioDevices = devices.filter(device => device.kind === 'audiooutput');
  await audio.setSinkId(audioDevices[0].deviceId);
}

const fs_wrapper  = document.getElementById('fs-wrapper');
const cta_wrapper = document.getElementById('cta-wrapper');
const ringer      = document.getElementById('ringer');
const message     = document.getElementById('message');
const call_wrapper= document.getElementById('call-wrapper');
const cta_btn_msg = 'Listen to AC Moideen';

$(document).ready(function(){
	$('.cta-stop').hide();
	$('.cta').show();
	
	$(cta_wrapper).hide();
	$(call_wrapper).hide();
	
	// on sc1 cta clicked
	$('.cta').on('click', function(e){
		e.preventDefault();
		
		if(screenfull.isEnabled) {
			screenfull.request(fs_wrapper);
			$(cta_wrapper).show();
		} else {
		    
		    message.currentTime = 0;
		    message.play();
		    $(this).hide();
		    $('.cta-stop').show();
		    return false;
		}
		
		// start ringer
		ringer.play();
		
	});
	
	$('.cta-stop').on('click',function(e){
	    e.preventDefault();
	    
	    message.pause();
	    message.currentTime = 0;
	    $(this).hide();
	    $('.cta').show();
	});
	
	$('.pickup').on('click', function(e){
		e.preventDefault();
		
		message.currentTime = 0;

		$(cta_wrapper).hide();
		$(call_wrapper).show();
		// stop ringer
		ringer.pause();
		// start audio
		message.play();
		
	});
	
	$('.hangup').on('click', function(e){
		e.preventDefault();
		
		console.log('hanging up');
		
		// stop & reset audio
		ringer.pause();
		message.pause();
		ringer.currentTime = 0;
		message.currentTime = 0;
		$(cta_wrapper).hide();
		$(call_wrapper).hide();
		
		console.log('isFS - ' + screenfull.isFullscreen);
		
		if(screenfull.isEnabled && screenfull.isFullscreen) {
		    screenfull.exit();
		    console.log('exiting FS');
		    exitFullScreen();
		}
		// set current_time = 0 msg
		
	});
	
	//document.getElementById('tracktime').innerHTML = Math.floor(this.currentTime) + ' / ' + Math.floor(this.duration);
	$(message).on('timeupdate', function(e){
		var ct = Math.floor(this.currentTime)
		var mmss = new Date(ct * 1000).toISOString().substr(11, 8)
		$('#tracktime').html(mmss);
	});
	
	$(message).on('ended', function(e){
	    e.preventDefault();
	    
		console.log(this.duration);
		
		$('.cta').html(cta_btn_msg);
		$('.cta').removeClass('playing');
		message.currentTime = 0;
		$('.hangup').delay(10800).trigger('click');
		
	});
	
	//*
	if (screenfull.isEnabled) {
		screenfull.on('change', () => {
			console.log('Is fullscreen?', screenfull.isFullscreen ? 'Yes' : 'No');
			if(screenfull.isFullscreen == false) {
				
				//$('.hangup').trigger('click');
				screenfull.exit();
				ringer.pause();
				message.pause();
				$(cta_wrapper).hide();
				$(call_wrapper).hide();
			}
			return false;
		});
	}
	//* */
	
	function toggleFullScreen() {
      var doc = window.document;
      var docEl = doc.documentElement;
    
      var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
      var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
    
      if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
      }
      else {
        cancelFullScreen.call(doc);
      }
    }
    

});

function exitFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;
    
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    cancelFullScreen.call(doc);
}

(function () {
    function checkTime(i) {
        return (i < 10) ? "0" + i : i;
    }

    function startTime() {
        var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
        document.getElementById('current-time').innerHTML = h + ":" + m;
        document.getElementById('current-time2').innerHTML = h + ":" + m;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
})();
